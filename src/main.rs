pub mod die_game;

use crate::die_game::die_hand::Hand;
use crate::die_game::rules::{compute_points, Rule, RuleType};
use crate::die_game::scoreboard::{assign_points, new_scoreboard, total_points, ScoreBoard};
use std::error::Error;

struct YahtzeeGame {
    hand: Hand,
    scoreboard: ScoreBoard,
}

impl YahtzeeGame {
    fn new() -> YahtzeeGame {
        let yahtzee_rules = vec![
            Rule {
                rule_type: RuleType::SameValue(1),
                name: String::from("Ones"),
            },
            Rule {
                rule_type: RuleType::SameValue(2),
                name: String::from("Twos"),
            },
            Rule {
                rule_type: RuleType::SameValue(3),
                name: String::from("Threes"),
            },
            Rule {
                rule_type: RuleType::SameValue(4),
                name: String::from("Fours"),
            },
            Rule {
                rule_type: RuleType::SameValue(5),
                name: String::from("Fives"),
            },
            Rule {
                rule_type: RuleType::SameValue(6),
                name: String::from("Sixes"),
            },
            Rule {
                rule_type: RuleType::ThreeOfAKind,
                name: String::from("Three of a kind"),
            },
            Rule {
                rule_type: RuleType::FourOfAKind,
                name: String::from("Four of a kind"),
            },
            Rule {
                rule_type: RuleType::FullHouse,
                name: String::from("Full house"),
            },
            Rule {
                rule_type: RuleType::SmallStraight,
                name: String::from("Small straight"),
            },
            Rule {
                rule_type: RuleType::LargeStraight,
                name: String::from("Large straight"),
            },
            Rule {
                rule_type: RuleType::Yahtzee,
                name: String::from("Yahtzee"),
            },
            Rule {
                rule_type: RuleType::Chance,
                name: String::from("Chance"),
            },
        ];

        YahtzeeGame {
            hand: Hand::new(5, 6),
            scoreboard: new_scoreboard(yahtzee_rules),
        }
    }

    fn choose_dice(&mut self) -> Result<Vec<u8>, Box<dyn Error>> {
        println!("\nChoose which dice to re-roll (comma-separated index, or 'all') to continue:");
        let mut line = String::new();
        std::io::stdin().read_line(&mut line)?;

        let mut line = line.trim().to_lowercase();
        if String::from("all") == line {
            let v: Vec<u8> = vec![1, 2, 3, 4, 5];
            return Ok(v);
        }

        if line.len() == 0 {
            return Ok(vec![]);
        }

        // Perform some clean-up of input (Keep only numeric values and ',')
        line.retain(|s| s != ' ' && (s.is_numeric() || s == ','));
        let mut selection = Vec::new();
        for s in line.split(',') {
            if let Ok(i) = s.to_string().parse::<u8>() {
                if i == 0 || i > self.hand.len() as u8 {
                    let msg = format!("{} is out of range.", i);
                    return Err(Box::from(msg));
                }
                selection.push(i);
            } else {
                let msg = format!("Unable to get the dice selection.");
                return Err(Box::from(msg));
            }
        }
        return Ok(selection);
    }

    fn choose_scoring(&self) -> Result<u8, Box<dyn Error>> {
        self.show_scoreboard_points(true);
        println!("\nChoose which scoring to use:");
        loop {
            let mut line = String::new();
            std::io::stdin().read_line(&mut line)?;
            let parsed = line.trim().parse::<u8>();
            match parsed {
                Ok(row) => {
                    if row < 1 || row > self.scoreboard.len() as u8 {
                        println!("Please select an existing scoring rule.");
                    } else {
                        return Ok(row);
                    }
                }
                Err(_) => println!("You entered something other than a number. Please try again."),
            }
        }
    }

    fn show_scoreboard_points(&self, hint: bool) {
        println!("\nSCOREBOARD");
        println!("===================================");
        println!("{}", self.create_points_overview(hint));
        println!("===================================");
    }

    fn create_points_overview(&self, hint: bool) -> String {
        let mut overview = String::new();
        for (i, (r, s)) in self.scoreboard.iter().enumerate() {
            let possible_points = compute_points(&self.hand, &r.rule_type);
            let string_points = if hint && possible_points > 0 && *s == None {
                format!("{} ***", possible_points)
            } else {
                match *s {
                    Some(x) => format!("{}", x),
                    None => String::from("_"),
                }
            };
            let line = format!("{}. {}: {}\n", i + 1, r.name, string_points);
            overview.push_str(&line);
        }
        overview
    }

    fn do_turn(&mut self) -> Result<(), Box<dyn Error>> {
        let mut nb_rolls = 0;
        let mut selected_dice = vec![1, 2, 3, 4, 5];
        loop {
            // roll the dice
            println!("\nRolling dice...");
            self.hand.roll(&selected_dice);
            println!("{:?}", self.hand.get_hand());
            nb_rolls += 1;

            // if we reached the maximum number of rolls, we're done
            if nb_rolls >= 3 {
                break;
            }

            // choose which dice to reroll, break if empty
            selected_dice = self.choose_dice()?;
            if selected_dice.len() == 0 {
                break;
            }
        }

        loop {
            let selected_rule_index = self.choose_scoring()? as usize;

            let done = assign_points(&mut self.scoreboard, selected_rule_index, &mut self.hand);
            match done {
                Ok(_) => {
                    println!("Adding points to #{}", selected_rule_index);
                    break;
                }
                Err(e) => println!("{} Try again.", e),
            }
        }
        self.show_scoreboard_points(false);

        println!("\nPress any key to continue");
        let mut line = String::new();
        std::io::stdin().read_line(&mut line)?;
        clear_terminal();
        Ok(())
    }

    fn play(&mut self) -> Result<(), Box<dyn Error>> {
        // We keep going until the scoreboard is full
        for _ in 0..self.scoreboard.len() {
            self.do_turn()?;
        }
        println!("\nCongratulations! You finished the game!\n");
        self.show_scoreboard_points(false);
        println!("Total points: {}", total_points(&self.scoreboard));
        Ok(())
    }
}

fn clear_terminal() {
    // os.system('cls' if os.name == 'nt' else 'clear')
    if cfg!(windows) {
        std::process::Command::new("cls").status().unwrap();
    } else {
        std::process::Command::new("clear").status().unwrap();
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut game = YahtzeeGame::new();
    // println!("Size: {}", game.scoreboard.len());
    game.play()?;
    Ok(())
}
