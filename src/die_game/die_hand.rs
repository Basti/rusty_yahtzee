use rand::{thread_rng, Rng};

pub struct Die {
    nb_faces: u8,
    current_face: u8,
}

impl Die {
    pub fn new(sides: u8, face: Option<u8>) -> Die {
        if face == None {
            let mut die = Die {
                nb_faces: sides,
                current_face: sides,
            };
            die.roll();
            return die;
        } else {
            // raise an error if face > sides
            if face.unwrap() > sides {
                panic!(
                    "Face cannot be greater than the number of sides, got {}.",
                    face.unwrap()
                );
            }
            Die {
                nb_faces: sides,
                current_face: face.unwrap(),
            }
        }
    }

    pub fn nb_faces(&self) -> u8 {
        self.nb_faces
    }

    pub fn current_face(&self) -> u8 {
        self.current_face
    }

    pub fn roll(&mut self) {
        let mut rng = thread_rng();
        self.current_face = rng.gen_range(1, self.nb_faces + 1);
    }
}

pub struct Hand {
    // selected_dice: Vec<bool>,
    dice: Vec<Die>,
}

impl Hand {
    pub fn new(nb_dice: u8, die_sides: u8) -> Hand {
        let mut v: Vec<Die> = Vec::new();
        for _ in 1..=nb_dice {
            let d = Die::new(die_sides, None);
            v.push(d);
        }
        Hand {
            // selected_dice: vec![false; nb_dice as usize],
            dice: v,
        }
    }

    pub fn roll(&mut self, selected_dice: &Vec<u8>) {
        for i in selected_dice {
            if let Some(d) = self.dice.get_mut(*i as usize - 1) {
                d.roll();
            } else {
                panic!("Index out of range!");
            }
        }
    }

    pub fn get_hand(&self) -> Vec<u8> {
        let mut v = Vec::new();
        for d in &self.dice {
            v.push(d.current_face());
        }
        v
    }

    pub fn set_hand(&mut self, faces: Vec<u8>) {
        if faces.len() != self.len() {
            panic!(
                "The number of faces should be the size of the hand, got {}",
                faces.len()
            );
        }
        for (d, f) in self.dice.iter_mut().zip(faces.iter()) {
            d.current_face = f.to_owned();
        }
    }

    pub fn count(&self, face: u8) -> u32 {
        self.get_hand().iter().filter(|&n| *n == face).count() as u32
    }

    pub fn sum(&self) -> u32 {
        let hand: Vec<u32> = self.get_hand().iter().map(|&e| e as u32).collect();
        hand.iter().sum()
    }

    pub fn len(&self) -> usize {
        self.dice.len()
    }

    pub fn die_nb_faces(&self) -> u8 {
        self.dice[0].nb_faces
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic]
    fn die_creation_fail() {
        Die::new(8, Some(10));
    }

    #[test]
    fn die_creation() {
        let d = Die::new(6, Some(4));
        assert_eq!(d.nb_faces(), 6);
        assert_eq!(d.current_face(), 4);
    }

    #[test]
    fn set_die_face() {
        let mut d = Die::new(6, Some(3));
        d.current_face = 2;
        assert_eq!(d.current_face(), 2);
    }

    #[test]
    fn die_roll() {
        let mut d = Die::new(1, None);
        for _ in 0..50 {
            d.roll();
            assert_eq!(d.current_face, 1);
        }
    }

    #[test]
    fn set_hand() {
        let mut hand = Hand::new(5, 6);
        let v: Vec<u8> = vec![2, 2, 4, 4, 4];
        let vc = v.clone();
        hand.set_hand(v);
        assert_eq!(hand.get_hand(), vc);
    }

    #[test]
    #[should_panic]
    fn set_hand_fail() {
        let mut hand = Hand::new(5, 6);
        let v: Vec<u8> = vec![2, 2, 4, 4, 4, 3, 7];
        hand.set_hand(v);
    }

    #[test]
    fn hand_count() {
        let mut hand = Hand::new(5, 6);
        let v: Vec<u8> = vec![2, 2, 4, 4, 4];
        hand.set_hand(v);
        assert_eq!(hand.count(2), 2);
        assert_eq!(hand.count(4), 3);
        // let v: Vec<u8> = vec![2, 2, 4, 4, 4];
        // hand.set_hand(v);
        assert_eq!(hand.count(5), 0);
    }

    #[test]
    fn hand_sum() {
        let mut hand = Hand::new(5, 6);
        let v: Vec<u8> = vec![2, 2, 4, 4, 4];
        hand.set_hand(v);
        assert_eq!(hand.sum(), 16);
        let v: Vec<u8> = vec![4, 1, 5, 3, 1];
        hand.set_hand(v);
        assert_eq!(hand.sum(), 14);
    }

    #[test]
    fn hand_roll() {
        let mut hand = Hand::new(5, 1);
        let sel = vec![1, 2, 3, 4, 5];
        hand.roll(&sel);
        let v: Vec<u8> = vec![1, 1, 1, 1, 1];
        assert_eq!(hand.get_hand(), v);
    }

    #[test]
    #[should_panic]
    fn hand_roll_fail() {
        let mut hand = Hand::new(5, 1);
        let sel = vec![6];
        hand.roll(&sel);
    }
}
