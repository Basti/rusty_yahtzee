use crate::die_game::rules::Rule;
use crate::die_game::scoreboard::RuleError::{RuleAlreadyUsed, RuleNotExisting};
use crate::{compute_points, Hand};
use std::error::Error;
use std::fmt::{Display, Formatter};

pub type ScoreBoard = Vec<(Rule, Option<u32>)>;

#[derive(Debug, PartialEq)] // Allow the use of "{:?}" format specifier
pub enum RuleError {
    RuleAlreadyUsed,
    RuleNotExisting,
}

// Allow the use of "{}" format specifier
impl Display for RuleError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match *self {
            RuleAlreadyUsed => write!(f, "Rule Error"),
            RuleNotExisting => write!(f, "Rule Error"),
        }
    }
}

// Allow this type to be treated like an error
impl Error for RuleError {
    fn description(&self) -> &str {
        match *self {
            RuleAlreadyUsed => "This rule is already used!",
            RuleNotExisting => "This rule does not exist!",
        }
    }

    fn cause(&self) -> Option<&dyn Error> {
        match *self {
            RuleAlreadyUsed => None,
            RuleNotExisting => None,
        }
    }
}

pub fn new_scoreboard(rules: Vec<Rule>) -> ScoreBoard {
    let mut scoreboard = vec![];
    for r in rules {
        scoreboard.push((r, None));
    }
    scoreboard
}

pub fn assign_points(
    scoreboard: &mut ScoreBoard,
    rule_index: usize,
    hand: &Hand,
) -> Result<(), RuleError> {
    if let Some((rule, score)) = scoreboard.get_mut(rule_index - 1) {
        match score {
            Some(_) => Err(RuleAlreadyUsed),
            None => {
                *score = Some(compute_points(hand, &rule.rule_type));
                Ok(())
            }
        }
    } else {
        Err(RuleNotExisting)
    }
}

pub fn total_points(scoreboard: &ScoreBoard) -> u32 {
    scoreboard.iter().map(|(_, o)| o.unwrap_or(0)).sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::die_game::rules::{Rule, RuleType};

    fn get_some_rules() -> Vec<Rule> {
        let some_rules = vec![
            Rule {
                rule_type: RuleType::SameValue(1),
                name: String::from("Ones"),
            },
            Rule {
                rule_type: RuleType::SameValue(4),
                name: String::from("Fours"),
            },
            Rule {
                rule_type: RuleType::FullHouse,
                name: String::from("Full house"),
            },
        ];
        some_rules
    }

    #[test]
    fn scoreboard_creation() {
        let some_rules = get_some_rules();
        let sb = new_scoreboard(some_rules);

        assert_eq!(sb.len(), 3);
        for (_, s) in sb {
            assert_eq!(s, None);
        }
    }

    #[test]
    fn set_points() {
        let some_rules = get_some_rules();
        let mut sb = new_scoreboard(some_rules);

        let mut hand = Hand::new(5, 6);
        hand.set_hand(vec![6, 6, 6, 3, 3]);

        let result = assign_points(&mut sb, 12, &hand);
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), RuleNotExisting);

        let result = assign_points(&mut sb, 3, &hand);
        assert!(result.is_ok());

        let result = assign_points(&mut sb, 3, &hand);
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), RuleAlreadyUsed);

        for (r, s) in sb {
            if r.rule_type == RuleType::FullHouse {
                assert_eq!(s, Some(25));
            } else {
                assert_eq!(s, None);
            }
        }
    }

    #[test]
    fn compute_total() {
        let some_rules = get_some_rules();
        let mut sb = new_scoreboard(some_rules);

        let mut hand = Hand::new(5, 6);
        hand.set_hand(vec![6, 6, 6, 3, 3]);
        let _result = assign_points(&mut sb, 3, &hand);
        // assert!(result.is_ok());

        hand.set_hand(vec![1, 1, 1, 1, 3]);
        let _result = assign_points(&mut sb, 1, &hand);
        // assert!(result.is_ok());

        hand.set_hand(vec![4, 4, 1, 4, 3]);
        let _result = assign_points(&mut sb, 2, &hand);
        // assert!(result.is_ok());

        let total = total_points(&sb);
        assert_eq!(total, 41);
    }
}
