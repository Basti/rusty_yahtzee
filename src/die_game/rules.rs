use crate::die_game::die_hand::Hand;

#[derive(Eq, PartialEq, Hash, Copy, Clone)]
pub enum RuleType {
    SameValue(u8),
    ThreeOfAKind,
    FourOfAKind,
    FullHouse,
    SmallStraight,
    LargeStraight,
    Yahtzee,
    Chance,
}

#[derive(Eq, PartialEq, Hash, Clone)]
pub struct Rule {
    pub rule_type: RuleType,
    pub name: String,
}

fn same_val_points(hand: &Hand, value: &u8) -> u32 {
    let nb = hand.count(*value);
    nb * (*value) as u32
}

fn is_straight(list: &Vec<u8>) -> bool {
    // verify that the list has no duplicates
    let mut l_uniq = list.clone();
    l_uniq.sort();
    l_uniq.dedup();
    if list.len() != l_uniq.len() {
        return false;
    }
    // sum of consecutive n numbers 1...n = n * (n+1) / 2
    let min = list.iter().min().unwrap().to_owned() as u32;
    let max = list.iter().max().unwrap().to_owned() as u32;
    let consecutive_sum = (min + max) * (max - min + 1) / 2;
    // actual sum of the list
    let list_u32: Vec<u32> = list.iter().map(|&e| e as u32).collect();
    let sum: u32 = list_u32.iter().sum();

    sum == consecutive_sum
}

pub fn compute_points(hand: &Hand, rule: &RuleType) -> u32 {
    match rule {
        RuleType::SameValue(val) => same_val_points(hand, val),
        RuleType::ThreeOfAKind => {
            for i in 0..hand.die_nb_faces() {
                if hand.count(i + 1) >= 3 {
                    return hand.sum();
                }
            }
            0
        }
        RuleType::FourOfAKind => {
            for i in 0..hand.die_nb_faces() {
                if hand.count(i + 1) >= 4 {
                    return hand.sum();
                }
            }
            0
        }
        RuleType::FullHouse => {
            let counts: Vec<u32> = (0..hand.die_nb_faces())
                .map(|x| hand.count(x + 1))
                .collect();
            if counts.contains(&2) && counts.contains(&3) {
                25
            } else {
                0
            }
        }
        RuleType::SmallStraight => {
            let mut sorted_hand = hand.get_hand().clone();
            sorted_hand.sort();
            sorted_hand.dedup();
            if sorted_hand.len() == 4 && is_straight(&sorted_hand) {
                return 30;
            } else if sorted_hand.len() == 5
                && (is_straight(&sorted_hand[1..].to_owned())
                    || is_straight(&sorted_hand[..4].to_owned()))
            {
                return 30;
            } else {
                return 0;
            }
        }
        RuleType::LargeStraight => {
            if is_straight(&hand.get_hand()) {
                return 40;
            }
            0
        }
        RuleType::Yahtzee => {
            let mut sorted_hand = hand.get_hand().clone();
            sorted_hand.sort();
            sorted_hand.dedup();
            if sorted_hand.len() == 1 {
                return 50;
            }
            0
        }
        RuleType::Chance => hand.sum(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn same_value_rule() {
        let mut hand = Hand::new(5, 6);
        let h = vec![1, 1, 3, 1, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::SameValue(1)), 3);
        assert_eq!(compute_points(&hand, &RuleType::SameValue(3)), 3);
        assert_eq!(compute_points(&hand, &RuleType::SameValue(5)), 5);
        assert_eq!(compute_points(&hand, &RuleType::SameValue(6)), 0);

        let h = vec![2, 2, 4, 4, 4];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::SameValue(2)), 4);
        assert_eq!(compute_points(&hand, &RuleType::SameValue(4)), 12);
    }

    #[test]
    fn three_of_a_kind_rule() {
        let mut hand = Hand::new(5, 6);
        let h = vec![1, 1, 3, 1, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::ThreeOfAKind), 11);

        let h = vec![2, 2, 4, 4, 4];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::ThreeOfAKind), 16);

        let h = vec![2, 2, 5, 4, 4];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::ThreeOfAKind), 0);
    }

    #[test]
    fn four_of_a_kind_rule() {
        let mut hand = Hand::new(5, 6);
        let h = vec![1, 1, 1, 1, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::FourOfAKind), 9);

        let h = vec![2, 4, 4, 4, 4];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::FourOfAKind), 18);

        let h = vec![2, 2, 5, 4, 4];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::FourOfAKind), 0);
    }

    #[test]
    fn full_house_rule() {
        let mut hand = Hand::new(5, 6);
        let h = vec![1, 1, 1, 1, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::FullHouse), 0);

        let h = vec![2, 2, 4, 4, 4];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::FullHouse), 25);
    }

    #[test]
    fn small_straight_rule() {
        let mut hand = Hand::new(5, 6);
        let h = vec![1, 2, 3, 1, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::SmallStraight), 0);

        let h = vec![2, 4, 3, 4, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::SmallStraight), 30);

        let h = vec![2, 1, 3, 4, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::SmallStraight), 30);

        let h = vec![2, 1, 3, 4, 6];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::SmallStraight), 30);
    }

    #[test]
    fn large_straight_rule() {
        let mut hand = Hand::new(5, 6);
        let h = vec![1, 2, 3, 1, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::LargeStraight), 0);

        let h = vec![2, 4, 3, 4, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::LargeStraight), 0);

        let h = vec![2, 1, 3, 4, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::LargeStraight), 40);

        let h = vec![2, 4, 3, 6, 5];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::LargeStraight), 40);
    }

    #[test]
    fn yahtzee_rule() {
        let mut hand = Hand::new(5, 6);
        let h = vec![2, 2, 2, 2, 2];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::Yahtzee), 50);

        let h = vec![2, 2, 2, 4, 2];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::Yahtzee), 0);
    }

    #[test]
    fn chance_rule() {
        let mut hand = Hand::new(5, 6);
        let h = vec![2, 3, 2, 5, 3];
        hand.set_hand(h);
        assert_eq!(compute_points(&hand, &RuleType::Chance), 15);
    }
}
